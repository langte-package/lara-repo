<?php

// LaraRepoServiceProvider.php

namespace Langte\LaraRepo;

use Illuminate\Support\ServiceProvider;

class LaraRepoServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/lararepo.php' => config_path('lararepo.php'),
        ]);
    }

    // 註冊套件函式
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/lararepo.php', 'lararepo');

        $this->commands('Langte\LaraRepo\Console\Commands\MakeController');
        $this->commands('Langte\LaraRepo\Console\Commands\MakeService');
        $this->commands('Langte\LaraRepo\Console\Commands\MakeRepository');

        $this->app->singleton('LaraRepo', function ($app) {
            return new SayHello();
        });
    }
}
