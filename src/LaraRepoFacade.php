<?php

// LaraRepoFacade.php

namespace Langte\LaraRepo;

use Illuminate\Support\Facades\Facade;

class LaraRepoFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'LaraRepo';
    }
}
