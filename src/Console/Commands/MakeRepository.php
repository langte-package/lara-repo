<?php

namespace Langte\LaraRepo\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class MakeRepository extends Command
{
    const REPOSITORY_PATH = 'app/Repositories/';
    const REPOSITORY_STUB = __DIR__ . '/../../Stubs/Repository.stub';
    const BASE_REPOSITORY_STUB = __DIR__ . '/../../Stubs/BaseRepository.stub';
    const CONTRACT_PATH = 'app/Repositories/Contracts/';
    const CONTRACT_STUB = __DIR__ . '/../../Stubs/RepositoryContract.stub';

    protected $fs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repo {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $fs)
    {
        parent::__construct();
        $this->fs = $fs;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createRepository();
    }

    private function createRepository()
    {
        //region create base service
        $path = base_path(self::REPOSITORY_PATH) . 'RepositoryEloquent.php';

        if (!$this->fs->exists($path)) {
            // make base service file
            $this->makeDirectory($path);
            $stub = $this->fs->get(self::BASE_REPOSITORY_STUB);
            $this->fs->put($path, $stub);
            $this->info('Base repository created.');
        }
        //endregion create base service


        $name = $this->argument('name');

        $path = base_path(self::REPOSITORY_PATH . $name) . 'RepositoryEloquent.php';
        if ($this->fs->exists($path)) {
            $this->error('Repository already exists!');
            return;
        }

        $contractPath = base_path(self::CONTRACT_PATH . $name) . 'Repository.php';
        if ($this->fs->exists($contractPath)) {
            $this->error('Repository Contract already exists!');
            return;
        }

        // make repository file
        $this->makeDirectory($path);
        $stub = $this->fs->get(self::REPOSITORY_STUB);
        $stub = $this->renderStub($stub, ['name' => $name]);
        $this->fs->put($path, $stub);
        $this->info('Repository created successfully.');

        // make repository contract file
        $this->makeDirectory($contractPath);
        $stub = $this->fs->get(self::CONTRACT_STUB);
        $stub = $this->renderStub($stub, ['name' => $name]);
        $this->fs->put($contractPath, $stub);
        $this->info('Repository Contract created successfully.');

    }

    private function makeDirectory(string $path)
    {
        if (!$this->fs->isDirectory(dirname($path))) {
            $this->fs->makeDirectory(dirname($path), 0777, true, true);
        }
    }

    private function renderStub($stub, $data)
    {
        foreach ($data as $find => $replace) {
            $stub = str_replace('$' . $find, $replace, $stub);
        }
        return $stub;
    }
}
