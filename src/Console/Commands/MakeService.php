<?php

namespace Langte\LaraRepo\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class MakeService extends Command
{
    public const SERVICE_PATH = 'app/Services/';
    public const SERVICE_STUB = __DIR__ . '/../../Stubs/Service.stub';
    public const BASE_SERVICE_STUB = __DIR__ . '/../../Stubs/BaseService.stub';

    protected $fs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $fs)
    {
        parent::__construct();
        $this->fs = $fs;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createService();
    }

    private function createService()
    {
        //region create base service
        $path = base_path(self::SERVICE_PATH) . 'Service.php';

        if (!$this->fs->exists($path)) {
            // make base service file
            $this->makeDirectory($path);
            $stub = $this->fs->get(self::BASE_SERVICE_STUB);
            $this->fs->put($path, $stub);
            $this->info('Base service created.');
        }
        //endregion create base service

        $name = $this->argument('name');

        $path = base_path(self::SERVICE_PATH . $name) . 'Service.php';
        if ($this->fs->exists($path)) {
            $this->error('Service already exists!');
            return;
        }

        // make service file
        $this->makeDirectory($path);
        $stub = $this->fs->get(self::SERVICE_STUB);
        $stub = $this->renderStub($stub, ['name' => $name]);
        $this->fs->put($path, $stub);
        $this->info('Service created successfully.');
    }

    private function makeDirectory(string $path)
    {
        if (!$this->fs->isDirectory(dirname($path))) {
            $this->fs->makeDirectory(dirname($path), 0777, true, true);
        }
    }

    private function renderStub($stub, $data)
    {
        foreach ($data as $find => $replace) {
            $stub = str_replace('$' . $find, $replace, $stub);
        }
        return $stub;
    }
}
