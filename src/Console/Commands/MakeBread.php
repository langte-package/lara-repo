<?php

namespace Langte\LaraRepo\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class MakeBread extends Command
{
    const MODEL_PATH = 'app/Models/';
    const MODEL_STUB = __DIR__ . '/../../Stubs/Model.stub';
    const REPOSITORY_PATH = 'app/Http/Controllers/';
    const REPOSITORY_STUB = __DIR__ . '/../../Stubs/Controller.stub';
    const UNIT_TEST_PATH = 'tests/Unit/';
    const UNIT_TEST_STUB = __DIR__ . '/../../Stubs/UnitTest.stub';
    const BREAD_TEST_PATH = 'tests/Feature/';
    const BREAD_TEST_STUB = __DIR__ . '/../../Stubs/BreadTest.stub';
    const ROUTE_PATH = 'routes/api/';
    const ROUTE_STUB = __DIR__ . '/../../Stubs/Route.stub';

    protected $fs;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:bread {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new bread';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $fs)
    {
        parent::__construct();
        $this->fs = $fs;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $studlyName = Str::studly($name);
        $pluralName = Str::plural($name);
        $slugName = Str::slug(Str::kebab($name));
        $spaceName = Str::replace('-', ' ', $slugName);

        $this->createMigration($pluralName);
        $this->createModel($studlyName);
        $this->createFactory($studlyName);
        $this->createRequests($studlyName);
        $this->createResources($studlyName);
        // $this->createTests($studlyName, $slugName);
        $this->createRepository($studlyName);
        $this->createService($studlyName);
        $this->createController($studlyName, $spaceName);
        $this->createRoute($studlyName, $slugName);
    }

    private function createMigration(string $name): void
    {
        $snakeName = Str::snake(Str::studly($name));
        $migrationName = 'create_' . $snakeName . '_table';
        Artisan::call("make:migration $migrationName");
        $this->info('Migration created successfully.');
    }

    private function createModel(string $name): void
    {
        // Artisan::call("make:model $name");

        $path = base_path(self::MODEL_PATH . $name) . '.php';
        if ($this->fs->exists($path)) {
            $this->error('Model already exists!');
            return;
        }

        // make model file
        $this->makeDirectory($path);
        $stub = $this->fs->get(self::MODEL_STUB);
        $stub = $this->renderStub($stub, ['name' => $name]);
        $this->fs->put($path, $stub);
        $this->info('Model created successfully.');
    }

    private function createFactory(string $name): void
    {
        $factoryName = $name . 'Factory';
        Artisan::call("make:factory $factoryName --model=$name");
        $this->info('Factory created successfully.');
    }

    private function createRequests(string $name): void
    {
        $questName = $name . 'Request';

        Artisan::call("make:request $name/Store$questName");
        Artisan::call("make:request $name/Update$questName");
        $this->info('Requests created successfully.');
    }

    private function createResources(string $name): void
    {
        $resourceName = $name . 'Resource';
        $collectionResourceName = $name . 'CollectionResource';

        Artisan::call("make:resource $resourceName");
        Artisan::call("make:resource $collectionResourceName");
        $this->info('Resources created successfully.');
    }

    private function createTests(string $name, string $slugName): void
    {
        // $unitTestName = $name . 'Test';
        // $testName = $name . 'BreadTest';

        // Artisan::call("make:test $unitTestName --unit");
        // Artisan::call("make:test $name/$testName");
        // $this->info('Tests created successfully.');

        $path = base_path(self::UNIT_TEST_PATH . $name) . 'Test.php';
        if ($this->fs->exists($path)) {
            $this->error('Unit Test already exists!');
            return;
        }

        // make unit test file
        $this->makeDirectory($path);
        $stub = $this->fs->get(self::UNIT_TEST_STUB);
        $stub = $this->renderStub($stub, ['name' => $name]);
        $this->fs->put($path, $stub);
        $this->info('Unit Test created successfully.');

        $path = base_path(self::BREAD_TEST_PATH . $name) . '/' . $name . 'BreadTest.php';
        if ($this->fs->exists($path)) {
            $this->error('Feature Test already exists!');
            return;
        }

        // make unit test file
        $this->makeDirectory($path);
        $stub = $this->fs->get(self::BREAD_TEST_STUB);
        $stub = $this->renderStub($stub, ['name' => $name, 'slugName' => $slugName]);
        $this->fs->put($path, $stub);
        $this->info('Feature Test created successfully.');
    }

    private function createRepository(string $name)
    {
        Artisan::call("make:repo $name");
        $this->info('Repository created successfully.');
    }

    private function createService(string $name)
    {
        Artisan::call("make:service $name");
        $this->info('Service created successfully.');
    }

    private function createController(string $name, string $spaceName)
    {
        $path = base_path(self::REPOSITORY_PATH . $name) . 'Controller.php';
        if ($this->fs->exists($path)) {
            $this->error('Controller already exists!');
            return;
        }

        // make controller file
        $this->makeDirectory($path);
        $stub = $this->fs->get(self::REPOSITORY_STUB);
        $stub = $this->renderStub($stub, ['name' => $name, 'spaceName' => $spaceName]);
        $this->fs->put($path, $stub);
        $this->info('Controller created successfully.');
    }

    private function createRoute(string $name, string $slugName)
    {
        $path = base_path(self::ROUTE_PATH . $slugName) . '.php';
        if ($this->fs->exists($path)) {
            $this->error('Route already exists!');
            return;
        }

        // make route file
        $this->makeDirectory($path);
        $stub = $this->fs->get(self::ROUTE_STUB);
        $stub = $this->renderStub($stub, ['name' => $name, 'slugName' => $slugName]);
        $this->fs->put($path, $stub);
        $this->info('Route created successfully.');
    }

    private function makeDirectory(string $path)
    {
        if (!$this->fs->isDirectory(dirname($path))) {
            $this->fs->makeDirectory(dirname($path), 0777, true, true);
        }
    }

    private function renderStub($stub, $data)
    {
        foreach ($data as $find => $replace) {
            $stub = str_replace('$' . $find, $replace, $stub);
        }
        return $stub;
    }
}
