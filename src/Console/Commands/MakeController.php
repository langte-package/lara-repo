<?php

namespace Langte\LaraRepo\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class MakeController extends Command
{
    public const CONTROLLER_PATH = 'app/Http/Controllers/';
    public const CONTROLLER_WITH_RESOURCE_STUB = __DIR__ . '/../../Stubs/ControllerWithResource.stub';
    public const CONTROLLER_STUB = __DIR__ . '/../../Stubs/Controller.stub';

    protected $fs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:ctrl {name} {--resource}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $fs)
    {
        parent::__construct();
        $this->fs = $fs;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $studlyName = Str::studly($name);
        $slugName = Str::slug(Str::kebab($name));
        $spaceName = Str::replace('-', ' ', $slugName);

        $useResource = $this->option('resource');

        $this->createRequests($studlyName);

        if ($useResource) {
            $this->createResources($studlyName);
            $this->createController($studlyName, $spaceName, self::CONTROLLER_WITH_RESOURCE_STUB);
        } else {
            $this->createController($studlyName, $spaceName, self::CONTROLLER_STUB);
        }
    }

    private function createRequests(string $name): void
    {
        Artisan::call("make:request $name" . 'StoreRequest');
        Artisan::call("make:request $name" . 'UpdateRequest');
        $this->info('Requests created successfully.');
    }

    private function createResources(string $name): void
    {
        $resourceName = $name . 'Resource';
        $collectionResourceName = $name . 'CollectionResource';

        Artisan::call("make:resource $resourceName");
        Artisan::call("make:resource $collectionResourceName");
    }

    private function createController(string $name, string $spaceName, string $stubPath)
    {
        $name = $this->argument('name');
        $slugName = Str::slug(Str::kebab($name));
        $spaceName = Str::replace('-', ' ', $slugName);

        $path = base_path(self::CONTROLLER_PATH . $name) . 'Controller.php';
        if ($this->fs->exists($path)) {
            $this->error('Controller already exists!');
            return;
        }

        // make controller file
        $this->makeDirectory($path);
        $stub = $this->fs->get($stubPath);
        $stub = $this->renderStub($stub, ['name' => $name, 'spaceName' => $spaceName]);
        $this->fs->put($path, $stub);
        $this->info('Controller created successfully.');
    }

    private function makeDirectory(string $path)
    {
        if (!$this->fs->isDirectory(dirname($path))) {
            $this->fs->makeDirectory(dirname($path), 0777, true, true);
        }
    }

    private function renderStub($stub, $data)
    {
        foreach ($data as $find => $replace) {
            $stub = str_replace('$' . $find, $replace, $stub);
        }
        return $stub;
    }
}
