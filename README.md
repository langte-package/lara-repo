
## About Lara-Repo

Lara-repo is a Laravel RSP pattern generator base on [Laravel Repository](https://github.com/recca0120/laravel-repository). Lara-repo can automatically generate several patterns for Laravel projects, such as:

- Controller
- Service
- Repository

## Installation

```bash
composer require langte/lararepo @dev
```

Create `RepositoryServiceProvider`

```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * All of the container singletons that should be registered.
     *
     * @var array
     */
    public $singletons = [
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}

```



Add provider to `app.config`

```php

'providers' => [
    ...
    App\Providers\RepositoryServiceProvider::class,
]

```

## Usage


### Controller Generator

Create controller
```bash
php artisan make:ctrl Demo
```

Create controller with resource, will auto create resource files
```bash
php artisan make:ctrl Demo --resource
```

### Service Generator

```bash
php artisan make:service Demo
```

If there is no `app\Services\Service.php` file, it will be automatically generated.

### Repository Generator


#### Create Repository

```bash
php artisan make:repo Demo

```
This commadn will create `app\Repositories\DemoRepositoryEloquent.php` and  `app\Repositories\Contracts\DemoRepository.php`

If there is no `app\Repositories\RepositoryEloquent.php` file, it will be automatically generated.

#### Add Single

```php
// app\Providders\RepositoryServiceProvider.php

public $singletons = [
    // ...
    \App\Repositories\Contracts\DemoRepository::class => \App\Repositories\DemoRepositoryEloquent::class,
];

```