<?php

namespace Tests;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected $enablesPackageDiscoveries = true;

    protected function getPackageProviders($app)
    {
        return [
            'Langte\LaraRepo',
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'LaraRepo' => 'Langte\LaraRepo',
        ];
    }

    // public function ignorePackageDiscoveriesFrom()
    // {
    //     return ['Langte\LaraRepo'];
    // }
}
